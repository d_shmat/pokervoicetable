package dmitry.shmat.pokerdemoversion;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static dmitry.shmat.pokerdemoversion.DataClass.numbersArr;
import static dmitry.shmat.pokerdemoversion.DataClass.suitArr;

public class ThreeCardsTableActivity extends AppCompatActivity implements View.OnClickListener {


    final int REQUEST_CODE = 1;
    int numStart = 0;
    int num = 1;
    boolean[] isEnabled;
    private Integer[] voiceArray;

    private TextView voiceInput;

    private ImageView card1_1;
    private ImageView card1_2;
    private ImageView card1_3;

    private ImageView card2_1;
    private ImageView card2_2;
    private ImageView card2_3;

    private ImageView card3_1;
    private ImageView card3_2;
    private ImageView card3_3;

    private ImageView card4_1;
    private ImageView card4_2;
    private ImageView card4_3;

    private ImageView card5_1;
    private ImageView card5_2;
    private ImageView card5_3;

    private ImageView card6_1;
    private ImageView card6_2;
    private ImageView card6_3;

    private ImageView card7_1;
    private ImageView card7_2;
    private ImageView card7_3;

    private ImageView card8_1;
    private ImageView card8_2;
    private ImageView card8_3;

    private ImageView card9_1;
    private ImageView card9_2;
    private ImageView card9_3;

    private ImageView card10_1;
    private ImageView card10_2;
    private ImageView card10_3;

    private Button bblock1;
    private Button bblock2;
    private Button bblock3;
    private Button bblock4;
    private Button bblock5;
    private Button bblock6;
    private Button bblock7;
    private Button bblock8;
    private Button bblock9;
    private Button bblock10;

    private RadioButton rad1;
    private RadioButton rad2;
    private RadioButton rad3;
    private RadioButton rad4;
    private RadioButton rad5;
    private RadioButton rad6;
    private RadioButton rad7;
    private RadioButton rad8;
    private RadioButton rad9;
    private RadioButton rad10;

    private EditText tcard1_1;
    private EditText tcard1_2;
    private EditText tcard1_3;

    private EditText tcard2_1;
    private EditText tcard2_2;
    private EditText tcard2_3;

    private EditText tcard3_1;
    private EditText tcard3_2;
    private EditText tcard3_3;

    private EditText tcard4_1;
    private EditText tcard4_2;
    private EditText tcard4_3;

    private EditText tcard5_1;
    private EditText tcard5_2;
    private EditText tcard5_3;

    private EditText tcard6_1;
    private EditText tcard6_2;
    private EditText tcard6_3;

    private EditText tcard7_1;
    private EditText tcard7_2;
    private EditText tcard7_3;

    private EditText tcard8_1;
    private EditText tcard8_2;
    private EditText tcard8_3;

    private EditText tcard9_1;
    private EditText tcard9_2;
    private EditText tcard9_3;

    private EditText tcard10_1;
    private EditText tcard10_2;
    private EditText tcard10_3;

    private ImageButton updateBut;
    private ImageButton deleteBut;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.three_cards_table_activity);

        isEnabled = new boolean[10];

        updateBut = (ImageButton) findViewById(R.id.update_But);
        updateBut.setOnClickListener(this);

        deleteBut = (ImageButton) findViewById(R.id.delete_But);
        deleteBut.setOnClickListener(this);

        bblock1 = (Button) findViewById(R.id.but1);
        bblock1.setOnClickListener(this);
        bblock2 = (Button) findViewById(R.id.but2);
        bblock2.setOnClickListener(this);
        bblock3 = (Button) findViewById(R.id.but3);
        bblock3.setOnClickListener(this);
        bblock4 = (Button) findViewById(R.id.but4);
        bblock4.setOnClickListener(this);
        bblock5 = (Button) findViewById(R.id.but5);
        bblock5.setOnClickListener(this);
        bblock6 = (Button) findViewById(R.id.but6);
        bblock6.setOnClickListener(this);
        bblock7 = (Button) findViewById(R.id.but7);
        bblock7.setOnClickListener(this);
        bblock8 = (Button) findViewById(R.id.but8);
        bblock8.setOnClickListener(this);
        bblock9 = (Button) findViewById(R.id.but9);
        bblock9.setOnClickListener(this);
        bblock10 = (Button) findViewById(R.id.but10);
        bblock10.setOnClickListener(this);

        voiceInput = (TextView) findViewById(R.id.voice_text);
        voiceInput.setOnClickListener(this);

        tcard1_1 = (EditText) findViewById(R.id.text1_1);
        tcard1_2 = (EditText) findViewById(R.id.text1_2);
        tcard1_3 = (EditText) findViewById(R.id.text1_3);

        tcard2_1 = (EditText) findViewById(R.id.text2_1);
        tcard2_2 = (EditText) findViewById(R.id.text2_2);
        tcard2_3 = (EditText) findViewById(R.id.text2_3);

        tcard3_1 = (EditText) findViewById(R.id.text3_1);
        tcard3_2 = (EditText) findViewById(R.id.text3_2);
        tcard3_3 = (EditText) findViewById(R.id.text3_3);

        tcard4_1 = (EditText) findViewById(R.id.text4_1);
        tcard4_2 = (EditText) findViewById(R.id.text4_2);
        tcard4_3 = (EditText) findViewById(R.id.text4_3);

        tcard5_1 = (EditText) findViewById(R.id.text5_1);
        tcard5_2 = (EditText) findViewById(R.id.text5_2);
        tcard5_3 = (EditText) findViewById(R.id.text5_3);

        tcard6_1 = (EditText) findViewById(R.id.text6_1);
        tcard6_2 = (EditText) findViewById(R.id.text6_2);
        tcard6_3 = (EditText) findViewById(R.id.text6_3);

        tcard7_1 = (EditText) findViewById(R.id.text7_1);
        tcard7_2 = (EditText) findViewById(R.id.text7_2);
        tcard7_3 = (EditText) findViewById(R.id.text7_3);

        tcard8_1 = (EditText) findViewById(R.id.text8_1);
        tcard8_2 = (EditText) findViewById(R.id.text8_2);
        tcard8_3 = (EditText) findViewById(R.id.text8_3);

        tcard9_1 = (EditText) findViewById(R.id.text9_1);
        tcard9_2 = (EditText) findViewById(R.id.text9_2);
        tcard9_3 = (EditText) findViewById(R.id.text9_3);

        tcard10_1 = (EditText) findViewById(R.id.text10_1);
        tcard10_2 = (EditText) findViewById(R.id.text10_2);
        tcard10_3 = (EditText) findViewById(R.id.text10_3);


        card1_1 = (ImageView) findViewById(R.id.card1_1);
        card1_1.setOnClickListener(this);

        card1_2 = (ImageView) findViewById(R.id.card1_2);
        card1_2.setOnClickListener(this);

        card1_3 = (ImageView) findViewById(R.id.card1_3);
        card1_3.setOnClickListener(this);

        card2_1 = (ImageView) findViewById(R.id.card2_1);
        card2_1.setOnClickListener(this);

        card2_2 = (ImageView) findViewById(R.id.card2_2);
        card2_2.setOnClickListener(this);

        card2_3 = (ImageView) findViewById(R.id.card2_3);
        card2_3.setOnClickListener(this);

        card3_1 = (ImageView) findViewById(R.id.card3_1);
        card3_1.setOnClickListener(this);

        card3_2 = (ImageView) findViewById(R.id.card3_2);
        card3_2.setOnClickListener(this);

        card3_3 = (ImageView) findViewById(R.id.card3_3);
        card3_3.setOnClickListener(this);

        card4_1 = (ImageView) findViewById(R.id.card4_1);
        card4_1.setOnClickListener(this);

        card4_2 = (ImageView) findViewById(R.id.card4_2);
        card4_2.setOnClickListener(this);

        card4_3 = (ImageView) findViewById(R.id.card4_3);
        card4_3.setOnClickListener(this);

        card5_1 = (ImageView) findViewById(R.id.card5_1);
        card5_1.setOnClickListener(this);

        card5_2 = (ImageView) findViewById(R.id.card5_2);
        card5_2.setOnClickListener(this);

        card5_3 = (ImageView) findViewById(R.id.card5_3);
        card5_3.setOnClickListener(this);

        card6_1 = (ImageView) findViewById(R.id.card6_1);
        card6_1.setOnClickListener(this);

        card6_2 = (ImageView) findViewById(R.id.card6_2);
        card6_2.setOnClickListener(this);

        card6_3 = (ImageView) findViewById(R.id.card6_3);
        card6_3.setOnClickListener(this);

        card7_1 = (ImageView) findViewById(R.id.card7_1);
        card7_1.setOnClickListener(this);

        card7_2 = (ImageView) findViewById(R.id.card7_2);
        card7_2.setOnClickListener(this);

        card7_3 = (ImageView) findViewById(R.id.card7_3);
        card7_3.setOnClickListener(this);

        card8_1 = (ImageView) findViewById(R.id.card8_1);
        card8_1.setOnClickListener(this);

        card8_2 = (ImageView) findViewById(R.id.card8_2);
        card8_2.setOnClickListener(this);

        card8_3 = (ImageView) findViewById(R.id.card8_3);
        card8_3.setOnClickListener(this);

        card9_1 = (ImageView) findViewById(R.id.card9_1);
        card9_1.setOnClickListener(this);

        card9_2 = (ImageView) findViewById(R.id.card9_2);
        card9_2.setOnClickListener(this);

        card9_3 = (ImageView) findViewById(R.id.card9_3);
        card9_3.setOnClickListener(this);

        card10_1 = (ImageView) findViewById(R.id.card10_1);
        card10_1.setOnClickListener(this);

        card10_2 = (ImageView) findViewById(R.id.card10_2);
        card10_2.setOnClickListener(this);

        card10_3 = (ImageView) findViewById(R.id.card10_3);
        card10_3.setOnClickListener(this);

        rad1 = (RadioButton) findViewById(R.id.radio1);
        rad1.setOnClickListener(this);
        rad2 = (RadioButton) findViewById(R.id.radio2);
        rad2.setOnClickListener(this);
        rad3 = (RadioButton) findViewById(R.id.radio3);
        rad3.setOnClickListener(this);
        rad4 = (RadioButton) findViewById(R.id.radio4);
        rad4.setOnClickListener(this);
        rad5 = (RadioButton) findViewById(R.id.radio5);
        rad5.setOnClickListener(this);
        rad6 = (RadioButton) findViewById(R.id.radio6);
        rad6.setOnClickListener(this);
        rad7 = (RadioButton) findViewById(R.id.radio7);
        rad7.setOnClickListener(this);
        rad8 = (RadioButton) findViewById(R.id.radio8);
        rad8.setOnClickListener(this);
        rad9 = (RadioButton) findViewById(R.id.radio9);
        rad9.setOnClickListener(this);
        rad10 = (RadioButton) findViewById(R.id.radio10);
        rad10.setOnClickListener(this);


        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.hide();

        PackageManager pm = getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(
                new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (activities.size() == 0) {
            voiceInput.setText(R.string.allert_text);
            voiceInput.setClickable(false);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.allert_title)
                    .setMessage(R.string.allert_text)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setCancelable(false)
                    .setNegativeButton(R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.voice_text:
                startVoiceRecognitionActivity();
                break;

            case R.id.but1:
                boolean click = isEnabled[0];
                tcard1_1.setEnabled(click);
                tcard1_1.setText("");
                tcard1_2.setEnabled(click);
                tcard1_2.setText("");
                tcard1_3.setEnabled(click);
                tcard1_3.setText("");
                if (click) {
                    card1_1.setImageResource(R.drawable.card_place);
                    card1_2.setImageResource(R.drawable.card_place);
                    card1_3.setImageResource(R.drawable.card_place);
                } else {
                    card1_1.setImageResource(R.drawable.card_place_pas);
                    card1_2.setImageResource(R.drawable.card_place_pas);
                    card1_3.setImageResource(R.drawable.card_place_pas);
                }
                isEnabled[0] = !isEnabled[0];
                break;

            case R.id.but2:
                boolean click1 = isEnabled[1];
                tcard2_1.setEnabled(click1);
                tcard2_1.setText("");
                tcard2_2.setEnabled(click1);
                tcard2_2.setText("");
                tcard2_3.setEnabled(click1);
                tcard2_3.setText("");
                if (click1) {
                    card2_1.setImageResource(R.drawable.card_place);
                    card2_2.setImageResource(R.drawable.card_place);
                    card2_3.setImageResource(R.drawable.card_place);
                } else {
                    card2_1.setImageResource(R.drawable.card_place_pas);
                    card2_2.setImageResource(R.drawable.card_place_pas);
                    card2_3.setImageResource(R.drawable.card_place_pas);
                }
                isEnabled[1] = !isEnabled[1];
                break;

            case R.id.but3:
                boolean click2 = isEnabled[2];
                tcard3_1.setEnabled(click2);
                tcard3_1.setText("");
                tcard3_2.setEnabled(click2);
                tcard3_2.setText("");
                tcard3_3.setEnabled(click2);
                tcard3_3.setText("");
                if (click2) {
                    card3_1.setImageResource(R.drawable.card_place);
                    card3_2.setImageResource(R.drawable.card_place);
                    card3_3.setImageResource(R.drawable.card_place);
                } else {
                    card3_1.setImageResource(R.drawable.card_place_pas);
                    card3_2.setImageResource(R.drawable.card_place_pas);
                    card3_3.setImageResource(R.drawable.card_place_pas);
                }
                isEnabled[2] = !isEnabled[2];
                break;

            case R.id.but4:
                boolean click3 = isEnabled[3];
                tcard4_1.setEnabled(click3);
                tcard4_1.setText("");
                tcard4_2.setEnabled(click3);
                tcard4_2.setText("");
                tcard4_3.setEnabled(click3);
                tcard4_3.setText("");
                if (click3) {
                    card4_1.setImageResource(R.drawable.card_place);
                    card4_2.setImageResource(R.drawable.card_place);
                    card4_3.setImageResource(R.drawable.card_place);
                } else {
                    card4_1.setImageResource(R.drawable.card_place_pas);
                    card4_2.setImageResource(R.drawable.card_place_pas);
                    card4_3.setImageResource(R.drawable.card_place_pas);
                }
                isEnabled[3] = !isEnabled[3];
                break;

            case R.id.but5:
                boolean click4 = isEnabled[4];
                tcard5_1.setEnabled(click4);
                tcard5_1.setText("");
                tcard5_2.setEnabled(click4);
                tcard5_2.setText("");
                tcard5_3.setEnabled(click4);
                tcard5_3.setText("");
                if (click4) {
                    card5_1.setImageResource(R.drawable.card_place);
                    card5_2.setImageResource(R.drawable.card_place);
                    card5_3.setImageResource(R.drawable.card_place);
                } else {
                    card5_1.setImageResource(R.drawable.card_place_pas);
                    card5_2.setImageResource(R.drawable.card_place_pas);
                    card5_3.setImageResource(R.drawable.card_place_pas);
                }
                isEnabled[4] = !isEnabled[4];
                break;

            case R.id.but6:
                boolean click5 = isEnabled[5];
                tcard6_1.setEnabled(click5);
                tcard6_1.setText("");
                tcard6_2.setEnabled(click5);
                tcard6_2.setText("");
                tcard6_3.setEnabled(click5);
                tcard6_3.setText("");
                if (click5) {
                    card6_1.setImageResource(R.drawable.card_place);
                    card6_2.setImageResource(R.drawable.card_place);
                    card6_3.setImageResource(R.drawable.card_place);
                } else {
                    card6_1.setImageResource(R.drawable.card_place_pas);
                    card6_2.setImageResource(R.drawable.card_place_pas);
                    card6_3.setImageResource(R.drawable.card_place_pas);
                }
                isEnabled[5] = !isEnabled[5];
                break;

            case R.id.but7:
                boolean click6 = isEnabled[6];
                tcard7_1.setEnabled(click6);
                tcard7_1.setText("");
                tcard7_2.setEnabled(click6);
                tcard7_2.setText("");
                tcard7_3.setEnabled(click6);
                tcard7_3.setText("");
                if (click6) {
                    card7_1.setImageResource(R.drawable.card_place);
                    card7_2.setImageResource(R.drawable.card_place);
                    card7_3.setImageResource(R.drawable.card_place);
                } else {
                    card7_1.setImageResource(R.drawable.card_place_pas);
                    card7_2.setImageResource(R.drawable.card_place_pas);
                    card7_3.setImageResource(R.drawable.card_place_pas);
                }
                isEnabled[6] = !isEnabled[6];
                break;

            case R.id.but8:
                boolean click7 = isEnabled[7];
                tcard8_1.setEnabled(click7);
                tcard8_1.setText("");
                tcard8_2.setEnabled(click7);
                tcard8_2.setText("");
                tcard8_3.setEnabled(click7);
                tcard8_3.setText("");
                if (click7) {
                    card8_1.setImageResource(R.drawable.card_place);
                    card8_2.setImageResource(R.drawable.card_place);
                    card8_3.setImageResource(R.drawable.card_place);
                } else {
                    card8_1.setImageResource(R.drawable.card_place_pas);
                    card8_2.setImageResource(R.drawable.card_place_pas);
                    card8_3.setImageResource(R.drawable.card_place_pas);
                }
                isEnabled[7] = !isEnabled[7];
                break;

            case R.id.but9:
                boolean click8 = isEnabled[8];
                tcard9_1.setEnabled(click8);
                tcard9_1.setText("");
                tcard9_2.setEnabled(click8);
                tcard9_2.setText("");
                tcard9_3.setEnabled(click8);
                tcard9_3.setText("");
                if (click8) {
                    card9_1.setImageResource(R.drawable.card_place);
                    card9_2.setImageResource(R.drawable.card_place);
                    card9_3.setImageResource(R.drawable.card_place);
                } else {
                    card9_1.setImageResource(R.drawable.card_place_pas);
                    card9_2.setImageResource(R.drawable.card_place_pas);
                    card9_3.setImageResource(R.drawable.card_place_pas);
                }
                isEnabled[8] = !isEnabled[8];
                break;

            case R.id.but10:
                boolean click9 = isEnabled[9];
                tcard10_1.setEnabled(click9);
                tcard10_1.setText("");
                tcard10_2.setEnabled(click9);
                tcard10_2.setText("");
                tcard10_3.setEnabled(click9);
                tcard10_3.setText("");
                if (click9) {
                    card10_1.setImageResource(R.drawable.card_place);
                    card10_2.setImageResource(R.drawable.card_place);
                    card10_3.setImageResource(R.drawable.card_place);
                } else {
                    card10_1.setImageResource(R.drawable.card_place_pas);
                    card10_2.setImageResource(R.drawable.card_place_pas);
                    card10_3.setImageResource(R.drawable.card_place_pas);
                }
                isEnabled[9] = !isEnabled[9];
                break;

            case R.id.update_But:
                fillingCards();
                break;

            case R.id.delete_But:
                cleanScreen();
                break;

            case R.id.card1_1:
                tcard1_1.requestFocus();
                break;

            case R.id.card1_2:
                tcard1_2.requestFocus();
                break;

            case R.id.card1_3:
                tcard1_3.requestFocus();
                break;

            case R.id.card2_1:
                tcard2_1.requestFocus();
                break;

            case R.id.card2_2:
                tcard2_2.requestFocus();
                break;

            case R.id.card2_3:
                tcard2_3.requestFocus();
                break;

            case R.id.card3_1:
                tcard3_1.requestFocus();
                break;

            case R.id.card3_2:
                tcard3_2.requestFocus();
                break;

            case R.id.card3_3:
                tcard3_3.requestFocus();
                break;

            case R.id.card4_1:
                tcard4_1.requestFocus();
                break;

            case R.id.card4_2:
                tcard4_2.requestFocus();
                break;

            case R.id.card4_3:
                tcard4_3.requestFocus();
                break;

            case R.id.card5_1:
                tcard5_1.requestFocus();
                break;

            case R.id.card5_2:
                tcard5_2.requestFocus();
                break;

            case R.id.card5_3:
                tcard5_3.requestFocus();
                break;

            case R.id.card6_1:
                tcard6_1.requestFocus();
                break;

            case R.id.card6_2:
                tcard6_2.requestFocus();
                break;

            case R.id.card6_3:
                tcard6_3.requestFocus();
                break;

            case R.id.card7_1:
                tcard7_1.requestFocus();
                break;

            case R.id.card7_2:
                tcard7_2.requestFocus();
                break;

            case R.id.card7_3:
                tcard7_3.requestFocus();
                break;

            case R.id.card8_1:
                tcard8_1.requestFocus();
                break;

            case R.id.card8_2:
                tcard8_2.requestFocus();
                break;

            case R.id.card8_3:
                tcard8_3.requestFocus();
                break;

            case R.id.card9_1:
                tcard9_1.requestFocus();
                break;

            case R.id.card9_2:
                tcard9_2.requestFocus();
                break;

            case R.id.card9_3:
                tcard9_3.requestFocus();
                break;

            case R.id.card10_1:
                tcard10_1.requestFocus();
                break;

            case R.id.card10_2:
                tcard10_2.requestFocus();
                break;

            case R.id.card10_3:
                tcard10_3.requestFocus();
                break;

            case R.id.radio1:
                num = 1;
                numStart = 1;
                setStartPosition(num);
                break;

            case R.id.radio2:
                num = 2;
                numStart = 2;
                setStartPosition(num);
                break;

            case R.id.radio3:
                num = 3;
                numStart = 3;
                setStartPosition(num);
                break;

            case R.id.radio4:
                num = 4;
                numStart = 4;
                setStartPosition(num);
                break;

            case R.id.radio5:
                num = 5;
                numStart = 5;
                setStartPosition(num);
                break;

            case R.id.radio6:
                num = 6;
                numStart = 6;
                setStartPosition(num);
                break;

            case R.id.radio7:
                num = 7;
                numStart = 7;
                setStartPosition(num);
                break;

            case R.id.radio8:
                num = 8;
                numStart = 8;
                setStartPosition(num);
                break;

            case R.id.radio9:
                num = 9;
                numStart = 9;
                setStartPosition(num);
                break;

            case R.id.radio10:
                num = 10;
                numStart = 0;
                setStartPosition(num);
                break;
        }

    }

    private void setStartPosition(int num) {
        for (int i = 0; i < 10; i++) {
            ((RadioButton) findViewById(DataClass.radioButtonArr[i])).setChecked(false);
        }
        ((RadioButton) findViewById(DataClass.radioButtonArr[num - 1])).setChecked(true);
    }

    private void startVoiceRecognitionActivity() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, R.string.voice_dialog);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            // Populate the wordsList with the String values the recognition engine thought it heard
            ArrayList<String> matches = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            String stroka = matches.get(0);

            parsingStroki(stroka);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void parsingStroki(String stroka) {
        int voiceArrayCounter = 0;
        int suitFound = -1;
        int numberFound = -1;
        String strokaArr[] = stroka.split(" ");
        if (strokaArr.length % 2 != 0) {
            Toast.makeText(this, "Ошибка распознавания голоса!", Toast.LENGTH_LONG).show();
        } else {
            voiceArray = new Integer[strokaArr.length / 2];
            for (int i = 0; i < strokaArr.length - 1; i += 2) {
                for (int x = 0; x < suitArr.length; x++) {
                    for (int y = 0; y < suitArr[x].length; y++) {
                        if (suitArr[x][y].equalsIgnoreCase(strokaArr[i].toLowerCase())) {
                            suitFound = x;
                            break;
                        }
                    }
                    if (suitFound != -1) break;
                }
                if (suitFound != -1) {
                    for (int j = 0; j < numbersArr.length; j++) {
                        for (int k = 0; k < numbersArr[j].length; k++) {
                            if (numbersArr[j][k].equalsIgnoreCase(strokaArr[i + 1].toLowerCase())) {
                                numberFound = j;
                                break;
                            }
                        }
                        if (numberFound != -1) break;
                    }
                }
                if (numberFound == -1 || suitFound == -1) {
                    Toast.makeText(this, "Ошибка распознавания голоса!", Toast.LENGTH_LONG).show();
                    break;
                } else {
                    voiceArray[voiceArrayCounter] = DataClass.compareArray[suitFound][numberFound][0];
                    voiceArrayCounter++;
                    numberFound = -1;
                    suitFound = -1;
                }
            }
            if (voiceArrayCounter == voiceArray.length) {
                String inputString = Arrays.toString(strokaArr);
                voiceInput.setText(inputString);
                fillingEdText();
            } else {
                String inputString = Arrays.toString(strokaArr);
                voiceInput.setText(inputString);
            }
        }

    }

    private void fillingEdText() {
        int amountActive = 0;
        int i = 0;
        int element = numStart;
        int lastActiveEditText = 9;
        for (int k = 0; k < 10; k++) {
            if (!isEnabled[k])
                amountActive++;
        }
        if (amountActive != voiceArray.length / 3 || voiceArray.length == 0) {
            voiceInput.setText(R.string.input_error);
        } else {
            while (isEnabled[lastActiveEditText])
                lastActiveEditText--;
            while (i < voiceArray.length) {
                Log.d("bla", String.format("element = %d", element));
                if (!isEnabled[element]) {
                    int row = -1;
                    if (i < voiceArray.length / 3) {
                        row = 0;
                    } else if (i < voiceArray.length * 2 / 3) {
                        row = 1;
                    } else row = 2;
                    ((EditText) findViewById(DataClass.editTextArr[row][element])).setText(voiceArray[i].toString());
                    Log.d("bla", String.format("[%d][%d] = %d", row, element, voiceArray[i]));
                    i++;
                }
                element++;
                Log.d("bla", String.format("pribavivliElement = %d", element));
                if (element > lastActiveEditText)
                    element = 0;
            }
            fillingCards();
        }
    }

    private void fillingCards() {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 10; j++) {
                if (((EditText) findViewById(DataClass.editTextArr[i][j])).getText().toString().equals("")) {
                } else {
                    int number = Integer.parseInt(((EditText) findViewById(DataClass.editTextArr[i][j])).getText().toString());
                    if (number < 53 & number >= 0) {
                        ((ImageView) findViewById(DataClass.imageViewArr[i][j])).setImageResource(DataClass.cardsArr[number]);
                    } else
                        Toast.makeText(this, "Неверный номер карты", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void cleanScreen() {
        for (int i = 0; i < 10; i++) {
            ((RadioButton) findViewById(DataClass.radioButtonArr[i])).setChecked(false);
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 10; j++) {
                ((EditText) findViewById(DataClass.editTextArr[i][j])).setText("");
                if (!isEnabled[j]) {
                    ((ImageView) findViewById(DataClass.imageViewArr[i][j])).setImageResource(R.drawable.card_place);
                }
                voiceInput.setText(R.string.central_text);
            }
        }
    }
}
