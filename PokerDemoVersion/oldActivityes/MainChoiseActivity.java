package dmitry.shmat.pokerdemoversion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;


public class MainChoiseActivity extends Activity implements View.OnClickListener {
    Intent choiseTableIntent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_choise_activity);

       /* Calendar c = Calendar.getInstance();
        c.clear();
        c.set(Calendar.YEAR, 2017);
        c.set(Calendar.MONTH, Calendar.AUGUST);
        c.set(Calendar.DAY_OF_MONTH, 10);
        if (!c.after(Calendar.getInstance()))
            finish();*/

        Button butTwoCards = (Button) findViewById(R.id.but_two_cards_field);
        butTwoCards.setOnClickListener(this);
        Button butThreeCards = (Button) findViewById(R.id.but_three_cards_field);
        butThreeCards.setOnClickListener(this);
        Button butFourCards = (Button) findViewById(R.id.but_four_cards_field);
        butFourCards.setOnClickListener(this);
        Button butFiveCards = (Button) findViewById(R.id.but_five_cards_field);
        butFiveCards.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.but_two_cards_field:
                choiseTableIntent = new Intent(this, TableActivity.class);
                startActivity(choiseTableIntent);
                choiseTableIntent = null;
                break;
            case R.id.but_three_cards_field:
                choiseTableIntent = new Intent(this, ThreeCardsTableActivity.class);
                startActivity(choiseTableIntent);
                choiseTableIntent = null;
                break;
            case R.id.but_four_cards_field:
                choiseTableIntent = new Intent(this, FourCardsTableActivity.class);
                startActivity(choiseTableIntent);
                choiseTableIntent = null;
                break;
            case R.id.but_five_cards_field:
                choiseTableIntent = new Intent(this, FiveCardsTableActivity.class);
                startActivity(choiseTableIntent);
                choiseTableIntent = null;
                break;
        }
    }
}
