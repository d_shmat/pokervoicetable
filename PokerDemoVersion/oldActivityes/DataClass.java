package dmitry.shmat.pokerdemoversion;

/**
 * Created by Дмитрий on 23.01.2017.
 */
public class DataClass {
    public static int[] cardsArr = {R.drawable.card00, R.drawable.card01, R.drawable.card02, R.drawable.card03,
            R.drawable.card04, R.drawable.card05, R.drawable.card06, R.drawable.card07,
            R.drawable.card08, R.drawable.card09, R.drawable.card10, R.drawable.card11,
            R.drawable.card12, R.drawable.card13, R.drawable.card14, R.drawable.card15,
            R.drawable.card16, R.drawable.card17, R.drawable.card18, R.drawable.card19,
            R.drawable.card20, R.drawable.card21, R.drawable.card22, R.drawable.card23,
            R.drawable.card24, R.drawable.card25, R.drawable.card26, R.drawable.card27,
            R.drawable.card28, R.drawable.card29, R.drawable.card30, R.drawable.card31,
            R.drawable.card32, R.drawable.card33, R.drawable.card34, R.drawable.card35,
            R.drawable.card36, R.drawable.card37, R.drawable.card38, R.drawable.card39,
            R.drawable.card40, R.drawable.card41, R.drawable.card42, R.drawable.card43,
            R.drawable.card44, R.drawable.card45, R.drawable.card46, R.drawable.card47,
            R.drawable.card48, R.drawable.card49, R.drawable.card50, R.drawable.card51,
            R.drawable.card52};

    public static int[] radioButtonArr = {R.id.radio1, R.id.radio2, R.id.radio3, R.id.radio4,
            R.id.radio5, R.id.radio6, R.id.radio7, R.id.radio8, R.id.radio9, R.id.radio10};

    public static int[][] editTextArr = {
            {R.id.text1_1, R.id.text2_1, R.id.text3_1, R.id.text4_1, R.id.text5_1, R.id.text6_1,
                    R.id.text7_1, R.id.text8_1, R.id.text9_1, R.id.text10_1},
            {R.id.text1_2, R.id.text2_2, R.id.text3_2, R.id.text4_2, R.id.text5_2, R.id.text6_2,
                    R.id.text7_2, R.id.text8_2, R.id.text9_2, R.id.text10_2},
            {R.id.text1_3, R.id.text2_3, R.id.text3_3, R.id.text4_3, R.id.text5_3, R.id.text6_3,
                    R.id.text7_3, R.id.text8_3, R.id.text9_3, R.id.text10_3},
            {R.id.text1_4, R.id.text2_4, R.id.text3_4, R.id.text4_4, R.id.text5_4, R.id.text6_4,
                    R.id.text7_4, R.id.text8_4, R.id.text9_4, R.id.text10_4},
            {R.id.text1_5, R.id.text2_5, R.id.text3_5, R.id.text4_5, R.id.text5_5, R.id.text6_5,
                    R.id.text7_5, R.id.text8_5, R.id.text9_5, R.id.text10_5}};

    public static int[][] imageViewArr = {
            {R.id.card1_1, R.id.card2_1, R.id.card3_1, R.id.card4_1, R.id.card5_1, R.id.card6_1,
                    R.id.card7_1, R.id.card8_1, R.id.card9_1, R.id.card10_1},
            {R.id.card1_2, R.id.card2_2, R.id.card3_2, R.id.card4_2, R.id.card5_2, R.id.card6_2,
                    R.id.card7_2, R.id.card8_2, R.id.card9_2, R.id.card10_2},
            {R.id.card1_3, R.id.card2_3, R.id.card3_3, R.id.card4_3, R.id.card5_3, R.id.card6_3,
                    R.id.card7_3, R.id.card8_3, R.id.card9_3, R.id.card10_3},
            {R.id.card1_4, R.id.card2_4, R.id.card3_4, R.id.card4_4, R.id.card5_4, R.id.card6_4,
                    R.id.card7_4, R.id.card8_4, R.id.card9_4, R.id.card10_4},
            {R.id.card1_5, R.id.card2_5, R.id.card3_5, R.id.card4_5, R.id.card5_5, R.id.card6_5,
                    R.id.card7_5, R.id.card8_5, R.id.card9_5, R.id.card10_5}};
}
